# graphql-ws Demo

Demo application providing a query, mutation and subscription using with support
for GraphQL over HTTP and Websocket.

## Up and Running

Start Phoenix endpoint with `mix phx.server` or inside IEx with `iex -S mix
phx.server`

You can also start a [GraphiQL](http://localhost:7080) workspace using `docker
compose up -d`.
