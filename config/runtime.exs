import Config

if System.get_env("PHX_SERVER") do
  config :graphql_ws_demo, GraphQLWSDemoWeb.Endpoint, server: true
end

if config_env() == :prod do
  host = System.get_env("PHX_HOST") || "localhost"
  port = String.to_integer(System.get_env("PORT") || "4000")

  config :graphql_ws_demo, GraphQLWSDemoWeb.Endpoint,
    url: [host: host, port: port, scheme: "http"],
    http: [
      # Enable IPv6 and bind on all interfaces.
      # Set it to  {0, 0, 0, 0, 0, 0, 0, 1} for local network only access.
      # See the documentation on https://hexdocs.pm/plug_cowboy/Plug.Cowboy.html
      # for details about using IPv6 vs IPv4 and loopback vs public addresses.
      ip: {0, 0, 0, 0, 0, 0, 0, 0},
      port: port
    ],
    secret_key_base:
      "dkO9MBf2u46ayoZpe/9Sm6UY8HC34AIuuXa79XysQFtOM5oAnptCnLAQpjdrOVHS"
end
