import Config

# For production, don't forget to configure the url host
# to something meaningful, Phoenix uses this information
# when generating URLs.

# Configures Swoosh API Client
# config :swoosh,
#   api_client: Swoosh.ApiClient.Finch,
#   finch_name: GraphQLWSDemo.Finch

# Do not print debug messages in production
config :logger, level: :info

# Runtime production configuration, including reading
# of environment variables, is done on config/runtime.exs.

# Do not log passwords and secrets in production
config :absinthe, Absinthe.Logger,
  filter_variables: ["authToken", "password", "secret", "token"]
