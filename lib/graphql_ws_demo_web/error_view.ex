defmodule GraphQLWSDemoWeb.ErrorView do
  alias Plug.Conn.Status

  def render(template, _assigns) do
    status =
      template
      |> String.split(".")
      |> hd()
      |> String.to_integer()

    message = Status.reason_phrase(status)

    %{errors: [%{message: message}]}
  end
end
