defmodule GraphQLWSDemoWeb.Schema do
  use GraphQLWSDemoWeb, :schema

  import_types GraphQLWSDemoWeb.Schema.Post

  query do
    import_fields :post_queries
  end

  mutation do
    import_fields :post_mutations
  end

  subscription do
    import_fields :post_subscriptions
  end
end
