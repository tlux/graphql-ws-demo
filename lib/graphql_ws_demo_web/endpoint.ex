defmodule GraphQLWSDemoWeb.Endpoint do
  use Phoenix.Endpoint, otp_app: :graphql_ws_demo
  use Absinthe.Phoenix.Endpoint

  # The websocket used for subscriptions
  socket "/subscriptions", GraphQLWSDemoWeb.GraphQLSocket,
    websocket: [
      path: "",
      subprotocols: ["graphql-transport-ws"]
    ]

  # Serve at "/" the static files from "priv/static" directory.
  #
  # You should set gzip to true if you are running phx.digest
  # when deploying your static files in production.
  plug Plug.Static,
    at: "/",
    from: :graphql_ws_demo,
    gzip: false,
    only: GraphQLWSDemoWeb.static_paths()

  # Code reloading can be explicitly enabled under the
  # :code_reloader configuration of your endpoint.
  if code_reloading? do
    plug Phoenix.CodeReloader
  end

  plug Plug.RequestId

  plug Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json, Absinthe.Plug.Parser],
    pass: ["*/*"],
    json_decoder: Phoenix.json_library()

  if Application.compile_env(:graphql_ws_demo, :dev_routes) do
    # We don't need a session in the production environment, but it is required
    # to operate Phoenix Live Dashboard
    @session_options [
      store: :cookie,
      key: "_graphql_ws_demo_key",
      signing_salt: "AMQnt1z5",
      same_site: "Lax"
    ]

    socket "/live", Phoenix.LiveView.Socket,
      websocket: [connect_info: [session: @session_options]]

    plug Plug.MethodOverride
    plug Plug.Head
    plug Plug.Session, @session_options
  end

  plug GraphQLWSDemoWeb.Router
end
