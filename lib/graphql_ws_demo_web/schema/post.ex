defmodule GraphQLWSDemoWeb.Schema.Post do
  use GraphQLWSDemoWeb, :schema_notation

  alias GraphQLWSDemoWeb.Resolvers.Post

  # Types

  object :post do
    field :id, non_null(:id)
    field :author, non_null(:string)
    field :body, non_null(:string)
  end

  # Queries

  object :post_queries do
    field :posts, non_null(list_of(non_null(:post))) do
      resolve &Post.list_posts/2
    end
  end

  # Mutations

  object :post_mutations do
    field :create_post, non_null(:post) do
      arg :author, non_null(:string)
      arg :body, non_null(:string)

      resolve &Post.create_post/2
    end
  end

  # Subscriptions

  object :post_subscriptions do
    field :post_created, non_null(:post) do
      config fn _args, _ ->
        {:ok, topic: "*", context_id: "global"}
      end

      trigger :create_post, topic: fn _ -> "*" end

      resolve fn post, _, _ ->
        {:ok, post}
      end
    end
  end
end
