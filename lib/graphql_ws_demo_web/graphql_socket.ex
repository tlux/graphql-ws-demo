defmodule GraphQLWSDemoWeb.GraphQLSocket do
  use Absinthe.GraphqlWS.Socket, schema: GraphQLWSDemoWeb.Schema
end
