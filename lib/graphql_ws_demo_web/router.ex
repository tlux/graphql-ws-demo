defmodule GraphQLWSDemoWeb.Router do
  use GraphQLWSDemoWeb, :router

  pipeline :api do
    plug Corsica, max_age: 600, origins: "*", allow_headers: :all
    plug :accepts, ["json"]
  end

  scope "/" do
    pipe_through :api

    forward "/", Absinthe.Plug,
      schema: GraphQLWSDemoWeb.Schema,
      json_codec: Phoenix.json_library()
  end
end
