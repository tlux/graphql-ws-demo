defmodule GraphQLWSDemoWeb.Resolvers.Post do
  alias GraphQLWSDemo.Posts

  def list_posts(_args, _resolution) do
    {:ok, Posts.list_posts()}
  end

  def create_post(args, _resolution) do
    Posts.create_post(args)
  end
end
