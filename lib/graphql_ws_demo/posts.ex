defmodule GraphQLWSDemo.Posts do
  use Agent

  alias GraphQLWSDemo.Posts.Post

  @capacity 5000

  def start_link(opts \\ []) do
    Agent.start_link(fn -> [] end, Keyword.put(opts, :name, __MODULE__))
  end

  def list_posts do
    Agent.get(__MODULE__, & &1)
  end

  def create_post(attrs) do
    with {:ok, post} <-
           Post
           |> struct(attrs)
           |> Map.put(:id, UUID.uuid4())
           |> Post.validate() do
      Agent.update(__MODULE__, fn posts ->
        Enum.take([post | posts], @capacity)
      end)

      {:ok, post}
    end
  end
end
