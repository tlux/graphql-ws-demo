defmodule GraphQLWSDemo.Posts.Post do
  defstruct [:id, :author, :body]

  def validate(%__MODULE__{author: ""}), do: {:error, "Author is blank"}
  def validate(%__MODULE__{body: ""}), do: {:error, "Body is blank"}
  def validate(%__MODULE__{} = post), do: {:ok, post}
end
