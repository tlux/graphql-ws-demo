defmodule GraphQLWSDemo.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the PubSub system
      {Phoenix.PubSub, name: GraphQLWSDemo.PubSub},
      # Start the Endpoint (http/https)
      GraphQLWSDemoWeb.Endpoint,
      # Enable Absinthe subscriptions on Phoenix Pubsub
      {Absinthe.Subscription, pubsub: GraphQLWSDemoWeb.Endpoint},
      GraphQLWSDemo.Posts
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: GraphQLWSDemo.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    GraphQLWSDemoWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
