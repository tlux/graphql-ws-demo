[
  import_deps: [:absinthe, :phoenix],
  subdirectories: ["priv/*/migrations"],
  inputs: ["*.{ex,exs}", "{config,lib,test}/**/*.{ex,exs}", "priv/*/seeds.exs"],
  line_length: 80,
  locals_without_parens: [
    connection: 1,
    connection: 2,
    object: 1
  ]
]
